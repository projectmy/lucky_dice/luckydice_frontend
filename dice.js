/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

const gREQUEST_STATUS_OK = 200;
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

//base url
const vBASE_URL = "http://42.115.221.44:8080/devcamp-lucky-dice/";
const vUTF8_TEXT_APPLICATION_HEADER = "application/json;charset=UTF-8";


// Tạo biến toàn cực đối tượng để truyền dữ liệu giữa các bước
const gUserObj = {
    username: "",
    firstname: "",
    lastname: ""
}
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

$(document).ready(function () {
    $('#btn-dice').on('click', function () {
        onBtnNemClick()
    })

});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnNemClick() {

    //  Bước 1 :  Thu thập dữ liệu
    getData(gUserObj)
    //    Bước 2 : Kiểm tra dữ liệu
    var vIsCheck = validateData(gUserObj);
    if (vIsCheck) {

        //   Bước 3 : gọi API sever
        callApiSeverDice(gUserObj);
    }
}




/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//CÁC HÀM XÚC XẮC
function getData(paramDataUser) {
    var vInputUserName = $("#inp_user_name");
    var vInputFirstName = $("#inp_first_name");
    var vInputLastName = $("#inp_last_name");

    paramDataUser.username = vInputUserName.val().trim();
    paramDataUser.firstname = vInputFirstName.val().trim();
    paramDataUser.lastname = vInputLastName.val().trim();

}

//         Hàm kiểm tra dữ liệu
function validateData(paramDataUser) {

    if (paramDataUser.username == "") {
        $('#inp_user_name').addClass("is-invalid").removeClass("is-valid");
        $('#user_name_false').html("Hãy nhập user name");
        console.log("Cần nhập User Name!!!");
        // alert("Cần nhập User Name!!!");
        return false;

    } else {
        $('#inp_user_name').addClass("is-valid").removeClass("is-invalid");
        $('#user_name_false').hide();
    }




    if (paramDataUser.firstname == "") {
        $('#inp_first_name').addClass("is-invalid").removeClass("is-valid");
        $('#first_name_false').html("Hãy nhập first name");
        console.log("Cần nhập First Name!!!");
        // alert("Cần nhập First Name!!!");
        return false;

    } else {
        $('#inp_first_name').addClass("is-valid").removeClass("is-invalid");
        $('#first_name_false').hide();
    }



    if (paramDataUser.lastname == "") {
        $('#inp_last_name').addClass("is-invalid").removeClass("is-valid");
        $('#last_name_false').html("Hãy nhập last name");
        console.log("Cần nhập Last Name!!!");
        // alert("Cần nhập Last Name!!!");
        return false;

    } else {
        $('#inp_last_name').addClass("is-valid").removeClass("is-invalid");
        $('#last_name_false').hide();
    }

    return true;
}


// Hàm call API
function callApiSeverDice(paramDataUser) {

    $.ajax({
        url: vBASE_URL + "/dice",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramDataUser),
        success: function (response) {
            console.log(response);

            //Hiển thị
            processResponse(response);
        },
        error: function (ajaxContent) {
            console.log(ajaxContent);
        }
    })
}



//      CÁC HÀM XỬ LÝ RESPONSE TRẢ VỀ THÀNH CÔNG
function processResponse(paramXmlHttpDice) {
    "user strict";

    console.log(paramXmlHttpDice);

    //  get result dice
    var vDiceReady = paramXmlHttpDice.dice;
    console.log("dice result: " + vDiceReady);

    // gọi hàm hiển thị dice
    changeDice(vDiceReady);

    //gọi hàm hiển thị lời nhắn
    changeThongBao(vDiceReady);

    //gọi hàm hiển thị voucher
    changeVoucherId(paramXmlHttpDice);

    //gọi hàm hiển thị ảnh phần thưởng
    changeGift(paramXmlHttpDice);
}

function changeDice(paramDice) {
    "user strict";
    var vImgDice = $("#img-dice");

    switch (paramDice) {
        case 1:
            vImgDice.attr("src", "images/1.png");
            break;
        case 2:
            vImgDice.attr("src", "images/2.png");
            break;
        case 3:
            vImgDice.attr("src", "images/3.png");
            break;
        case 4:
            vImgDice.attr("src", "images/4.png");
            break;
        case 5:
            vImgDice.attr("src", "images/5.png");
            break;
        case 6:
            vImgDice.attr("src", "images/6.png");
            break;
    }
}

function changeThongBao(paramDice) {
    "user strict";
    // hiển thị thẻ P chúc mừng
    var vElementPChucMung = $("#p_none");
    vElementPChucMung.css("display", "block");

    // Hiển thị lời chúc theo số điểm xúc xắc
    var vThongBao = $("#p-notification-dice");
    if (paramDice < 4) {
        vThongBao.html("Chúc bạn may mắn lần sau !!!");
        vThongBao.css("color", "blue");
    } else {
        vThongBao.html("Chúc mừng bạn hãy chơi tiếp lần nữa nha !!!")
        vThongBao.css({
            "color": "red",
            "font-weight": "bold"
        });
    }
}

function changeVoucherId(paramResponeVoucher) {
    "user strict";

    var vPVoucherId = $("#p-voucher-id");
    var vPVoucherPercent = $("#p-voucher-percent");

    if (paramResponeVoucher.voucher != null) {
        vPVoucherId.html(`<h3><b class="w3-text-deep-orange">ID: ${paramResponeVoucher.voucher.maVoucher}</b></h3>`);
        vPVoucherPercent.html(`<h3><b class="w3-text-deep-orange">Giảm giá: ${paramResponeVoucher.voucher.phanTramGiamGia}%</b></h3>`);
    } else {
        vPVoucherId.html(`<h3><b class="w3-text-deep-orange">ID: ....</b></h3>`);
        vPVoucherPercent.html(`<h3><b class="w3-text-deep-orange">Giảm giá: 0%</b></h3>`);
    }
}

function changeGift(paramResponePrize) {
    var vImgPrize = $("#img-prize");
    vImgPrize.attr("src", "images/no-present.jpg");
    var vTenPhanThuong = paramResponePrize.prize;
    switch (vTenPhanThuong) {
        case "Mũ":
            vImgPrize.attr("src", "images/hat.jpg");
            break;
        case "Áo":
            vImgPrize.attr("src", "images/t-shirt.jpg");
            break;
        case "Xe Máy":
            vImgPrize.attr("src", "images/motobike.jpg");
            break;
        case "Ô tô":
            vImgPrize.attr("src", "images/car.jpg");
            break;
        default:
            vImgPrize.attr("src", "images/no-present.jpg");
    }
}


//  HISTORY DICE -- LỊCH SỬ TUNG XÚC XẮC:

// hàm sự kiện nút lịch sử tung xúc xắc
$('#btn-dice-history').on('click', function () {

    console.log("%c Nút Dice History được ấn", "color:red");

    // khi ấn nút sẽ xóa dữ liệu có sẵn trong bảng
    var vTable = document.getElementById("history-placeholder-table")

    while (vTable.rows.length > 1) {
        vTable.deleteRow(1);
    }
    //  Bước 1 :  Thu thập dữ liệu
    getData(gUserObj);

    //    Bước 2 : Kiểm tra dữ liệu
    var vIsCheck = validateData(gUserObj);
    if (vIsCheck) {
        var vXmlHttpDiceHistory = new XMLHttpRequest();

        //   Bước 3 : gọi API sever
        callApiHistoryDice(vXmlHttpDiceHistory, gUserObj);

        vXmlHttpDiceHistory.onreadystatechange = function () {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) { // Nếu nhận trạng thái của reponse ready và ok
                //   Bước 4 : xử lý response trả về sever
                console.log(vXmlHttpDiceHistory.responseText);
                var vReponsiveHistoryDice = JSON.parse(vXmlHttpDiceHistory.responseText);
                console.log(vReponsiveHistoryDice);
                showDiceHistory(vReponsiveHistoryDice);
            }
        }
    }
})

// Hàm gọi API lấy lịch xử tung xức xắc từ sever
function callApiHistoryDice(paramXmlHttpDiceHistory, paramDataUser) {
    paramXmlHttpDiceHistory.open("GET", vBASE_URL + "/dice-history?username=" + paramDataUser.username, true)
    paramXmlHttpDiceHistory.setRequestHeader("Content-Type", vUTF8_TEXT_APPLICATION_HEADER);
    paramXmlHttpDiceHistory.send(JSON.stringify(paramDataUser)); //    Chuyển JSON object sang string
}

// Hàm hiển thị lịch sử tung xúc xắc
function showDiceHistory(paramResposeDiceHistory) {
    // Table Header
    var vTable = document.getElementById("history-placeholder-table");
    vTable.rows[0].cells[0].innerHTML = "Lượt";
    vTable.rows[0].cells[1].innerHTML = "Kết quả";

    // hiển thị lịch sử ra tbody
    var vTbody = vTable.getElementsByTagName("tbody")[0];
    vTbody.innerHTML = "";

    for (let bI = 0; bI < paramResposeDiceHistory.dices.length; bI++) {
        var vNewRow = vTbody.insertRow(-1);
        var vNewNumCells = vNewRow.insertCell(0);
        var vNewDiceCells = vNewRow.insertCell(1);

        // hiển thị ra HTML
        vNewNumCells.innerHTML = bI + 1;
        vNewDiceCells.innerHTML = paramResposeDiceHistory.dices[bI];
    }
}

//  HISTORY VOUCHERS -- LỊCH SỬ VOUCHERS:

// hàm sự kiện nút lịch sử voucher
$('#btn-voucher-history').on('click', function () {

    console.log("%c Nút Vouchers History được ấn", "color:red");

    // khi ấn nút sẽ xóa dữ liệu có sẵn trong bảng
    var vTable = document.getElementById("history-placeholder-table")

    while (vTable.rows.length > 1) {
        vTable.deleteRow(1);
    }
    //  Bước 1 :  Thu thập dữ liệu
    getData(gUserObj);

    //    Bước 2 : Kiểm tra dữ liệu
    var vIsCheck = validateData(gUserObj);
    if (vIsCheck) {
        var vXmlHttpVoucherHistory = new XMLHttpRequest();

        //   Bước 3 : gọi API sever
        callApiHistoryVoucher(vXmlHttpVoucherHistory, gUserObj);

        vXmlHttpVoucherHistory.onreadystatechange = function () {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) { // Nếu nhận trạng thái của reponse ready và ok
                //   Bước 4 : xử lý response trả về sever
                console.log(vXmlHttpVoucherHistory.responseText);
                var vReponsiveHistoryVoucher = JSON.parse(vXmlHttpVoucherHistory.responseText);
                console.log(vReponsiveHistoryVoucher);
                showVoucherHistory(vReponsiveHistoryVoucher);
            }
        }
    }
})

// Hàm gọi API lấy lịch xử voucher từ sever
function callApiHistoryVoucher(paramXmlHttpVoucherHistory, paramDataUser) {
    paramXmlHttpVoucherHistory.open("GET", vBASE_URL + "/voucher-history?username=" + paramDataUser.username, true)
    paramXmlHttpVoucherHistory.setRequestHeader("Content-Type", vUTF8_TEXT_APPLICATION_HEADER);
    paramXmlHttpVoucherHistory.send(JSON.stringify(paramDataUser)); //    Chuyển JSON object sang string
}

// Hàm hiển thị lịch sử voucher
function showVoucherHistory(paramResposeVoucherHistory) {
    // Table Header
    var vTable = document.getElementById("history-placeholder-table");
    vTable.rows[0].cells[0].innerHTML = "Mã nhận thưởng";
    vTable.rows[0].cells[1].innerHTML = "Phần trăm giảm giá";

    // hiển thị lịch sử ra tbody
    var vTbody = vTable.getElementsByTagName("tbody")[0];
    vTbody.innerHTML = "";

    for (let bI = 0; bI < paramResposeVoucherHistory.vouchers.length; bI++) {
        var vNewRow = vTbody.insertRow(-1);
        var vNewMaVoucherCells = vNewRow.insertCell(0);
        var vNewDiscountCells = vNewRow.insertCell(1);

        // hiển thị ra HTML
        vNewMaVoucherCells.innerHTML = paramResposeVoucherHistory.vouchers[bI].maVoucher;
        vNewDiscountCells.innerHTML = paramResposeVoucherHistory.vouchers[bI].phanTramGiamGia + "%";
    }
}


//  HISTORY VOUCHERS -- LỊCH SỬ VOUCHERS:

// hàm sự kiện nút lịch sử voucher
$('#btn-prize-history').on('click', function () {

    console.log("%c Nút Prize History được ấn", "color:red");

    // khi ấn nút sẽ xóa dữ liệu có sẵn trong bảng
    var vTable = document.getElementById("history-placeholder-table")

    while (vTable.rows.length > 1) {
        vTable.deleteRow(1);
    }
    //  Bước 1 :  Thu thập dữ liệu
    getData(gUserObj);

    //    Bước 2 : Kiểm tra dữ liệu
    var vIsCheck = validateData(gUserObj);
    if (vIsCheck) {
        var vXmlHttpPrizeHistory = new XMLHttpRequest();

        //   Bước 3 : gọi API sever
        callApiHistoryPrize(vXmlHttpPrizeHistory, gUserObj);

        vXmlHttpPrizeHistory.onreadystatechange = function () {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) { // Nếu nhận trạng thái của reponse ready và ok
                //   Bước 4 : xử lý response trả về sever
                console.log(vXmlHttpPrizeHistory.responseText);
                var vReponsiveHistoryPrize = JSON.parse(vXmlHttpPrizeHistory.responseText);
                console.log(vReponsiveHistoryPrize);
                showVoucherPrize(vReponsiveHistoryPrize);
            }
        }
    }
})

// Hàm gọi API lấy lịch xử voucher từ sever
function callApiHistoryPrize(paramXmlHttpPrizeHistory, paramDataUser) {
    paramXmlHttpPrizeHistory.open("GET", vBASE_URL + "/prize-history?username=" + paramDataUser.username, true)
    paramXmlHttpPrizeHistory.setRequestHeader("Content-Type", vUTF8_TEXT_APPLICATION_HEADER);
    paramXmlHttpPrizeHistory.send(JSON.stringify(paramDataUser)); //    Chuyển JSON object sang string
}

// Hàm hiển thị lịch sử voucher
function showVoucherPrize(paramResposeVoucherHistory) {
    // Table Header
    var vTable = document.getElementById("history-placeholder-table");
    vTable.rows[0].cells[0].innerHTML = "Lượt";
    vTable.rows[0].cells[1].innerHTML = "Phần thưởng";

    // hiển thị lịch sử ra tbody
    var vTbody = vTable.getElementsByTagName("tbody")[0];
    vTbody.innerHTML = "";

    for (let bI = 0; bI < paramResposeVoucherHistory.prizes.length; bI++) {
        var vNewRow = vTbody.insertRow(-1);
        var vNewMaVoucherCells = vNewRow.insertCell(0);
        var vNewDiscountCells = vNewRow.insertCell(1);

        // hiển thị ra HTML
        vNewMaVoucherCells.innerHTML = bI + 1;
        vNewDiscountCells.innerHTML = paramResposeVoucherHistory.prizes[bI];
    }
}